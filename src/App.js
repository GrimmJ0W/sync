import Header from "./features/Header";
import GreetingsPage from "./pages/GreetingsPage";
import DemoExamplePage from "./pages/DemoExamplePage";


function App() {
    return (
        <>
            <Header/>
            <GreetingsPage />
            <DemoExamplePage />
        </>
    );
}

export default App;
