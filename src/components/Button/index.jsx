import React from 'react';
import {StyledButton} from "./style";
import {Icon} from 'semantic-ui-react'

const Button = ({width, marginTop, type, children}) => {
    return (
        <StyledButton width={width} marginTop={marginTop} type={type}>
            <div>
                {children}
            </div>
            <Icon name='long arrow alternate right'  />
        </StyledButton>
    );
};

export default Button;
