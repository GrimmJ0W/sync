import styled from "styled-components";

export const StyledButton = styled.div`
  width: ${(p) => {
    return p.width
  }};
  color: ${(p) => {
    return p.type === 'primary' ? '#fff' : 'rgba(0, 0, 0, 1)'
  }};
  background: ${(p) => {
    return p.type === 'primary' ? 'rgba(0, 0, 0, 1)' : '#fff'
  }};
  border-radius: 10px;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 8px 16px;
  margin-top: ${(p) => {
    return p.marginTop
  }};
  
  div:first-child {
    margin-right: 8px;
  }
`

