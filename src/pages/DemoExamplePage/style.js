import styled from "styled-components";

export const DemoExamplePageTittle = styled.div`
  margin-top: 355px;
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  justify-content: center;
  font-weight: 400;
  font-size: 24px;
  line-height: 36px;
  padding: 0 103px;  
`
