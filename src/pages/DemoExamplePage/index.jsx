import React from 'react';
import {DemoExamplePageTittle} from "./style";
import {getGradientText} from "../../utils/getGradientText";



const DemoExamplePage = () => {
    return (
        <>
            <DemoExamplePageTittle>
                Synchronizd is a meeting scheduler that considers everyone’s preferences to find
                the&nbsp; <b>perfect</b> &nbsp;time, <i>every</i>&nbsp; time,&nbsp; {getGradientText('automatically')}.
            </DemoExamplePageTittle>
        </>
    );
};

export default DemoExamplePage;
