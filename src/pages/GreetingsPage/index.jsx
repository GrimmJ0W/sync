import React from 'react';
import {GreetingsPageContainer, GreetingsPageText, GreetingsPageTittle} from "./style";
import {getGradientText} from "../../utils/getGradientText";
import Button from "../../components/Button";


const GreetingsPage = () => {
    return (
        <GreetingsPageContainer>
            <GreetingsPageTittle>
                The&nbsp;{getGradientText('Scheduling')}&nbsp;Engine
            </GreetingsPageTittle>
            <GreetingsPageText>
                Get busy people on your calendar <i>fast</i> by making responding to your calendar invites, and rescheduling them, <b>effortless.</b>
            </GreetingsPageText>
            <Button type='primary' width='160px' marginTop='54px'>
                Request Access
            </Button>
        </GreetingsPageContainer>
    );
};

export default GreetingsPage;
