import styled from "styled-components";

export const GreetingsPageContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`

export const GreetingsPageTittle = styled.div`
  font-size: 50px;
  font-weight: 600;
  line-height: 75px;
  margin-top: 80px;
  display: flex;
`

export const GreetingsPageText = styled.div`
  margin-top: 16px;
  font-weight: 400;
  font-size: 30px;
  line-height: 45px;
  text-align: center;
  padding: 0 136px;
`



