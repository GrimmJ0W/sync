import styled from 'styled-components'

const TextWithGradient = styled.div`  
  background: -webkit-linear-gradient(left, rgba(80, 94, 255, 1) , rgba(245, 88, 109, 1));
  background: -o-linear-gradient(right, rgba(80, 94, 255, 1), rgba(245, 88, 109, 1));
  background: -moz-linear-gradient(right, rgba(80, 94, 255, 1), rgba(245, 88, 109, 1));
  background: linear-gradient(to right, rgba(80, 94, 255, 1) , rgba(245, 88, 109, 1));
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;
`

export const getGradientText = (text) => {
    return (
        <TextWithGradient>
            {text}
        </TextWithGradient>
    )
}