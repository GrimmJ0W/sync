import styled from 'styled-components'

export const HeaderWrapper = styled.div`  
  display: flex;
  justify-content: space-between;
  padding: 37px 50px 0 46px;
`

export const HeaderTittle = styled.div`
  font-size: 32px;
  line-height: 48px;
  font-weight: 600;
  display: flex;
  align-items: center;
  justify-content: center;
`

export const HeaderHint = styled.div`
  display: flex;
  font-size: 16px;
  line-height: 24px;
  font-weight: 400;
  align-items: center;
  justify-content: center;
  
  div:last-child{
    margin-left: 16px;
  }

`

