import React from 'react';
import {HeaderHint, HeaderTittle, HeaderWrapper} from "./style";
import {getGradientText} from "../../utils/getGradientText";

const Header = () => {
    return (
        <HeaderWrapper>
            <HeaderTittle>
                {getGradientText('SYNC')}HRONIZD
            </HeaderTittle>
            <HeaderHint>
                <div>How it works</div>
                <div>Privacy</div>
            </HeaderHint>
        </HeaderWrapper>
    );
};

export default Header;
